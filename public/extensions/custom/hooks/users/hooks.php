<?php

return [
    'filters' => [
        'item.create.directus_users:before' => function (\Directus\Hook\Payload $payload) {
            $roleId = $payload->get('role');
            if (!$roleId) {
                throw new \Directus\Exception\UnprocessableEntityException('User role id is required!');
            } elseif (intval($roleId) != 3) {
                throw new \Directus\Exception\UnprocessableEntityException('Invalid role id!');
            }

            return $payload;
        }
    ]
];